import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorage";

export default function UserNav() {
  let userData = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogOut = () => {
    userLocalStorage.remove();
    window.location.href = "/login";
  };

  const renderUserNav = () => {
    if (userData) {
      return (
        <>
          <span>{userData.hoTen}</span>
          <button
            className="border-2 border-black rounded px-5 py-2"
            onClick={handleLogOut}
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="rounded px-5 py-2 bg-cyan-600 text-white">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to={"/signup"}>
            <button className="rounded px-5 py-2 bg-red-500 text-white">
              Đăng ký
            </button>
          </NavLink>
        </>
      );
    }
  };

  return <div className="space-x-5 user-nav">{renderUserNav()}</div>;
}
