import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { carouselService } from "../../services/carouselService";

export default function HomeCarousel() {
  const { carouselImg } = useSelector((state) => state.carouselReducer);

  // //////////////////////////////////
  // Khi nào dùng carousel img từ api trả về thì uncomment dòng 14 đến 28 :)))))
  // Lý do: ảnh từ api quá củ chuối
  // /////////////////////////////////

  const dispatch = useDispatch();

  useEffect(() => {
    carouselService
      .getCarouselImg()
      .then((res) => {
        dispatch({
          type: "SET_CAROUSEL",
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });

  let renderCarousel = () => {
    const contentStyle = {
      height: "70vh",
      color: "#fff",
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
    };

    return carouselImg.map((item, index) => {
      return (
        <div key={index} className="home-carousel">
          <div
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img src={item.hinhAnh} className="w-full opacity-0" alt="banner" />
          </div>
        </div>
      );
    });
  };

  return (
    <Carousel autoplay className="mb-20">
      {renderCarousel()}
    </Carousel>
  );
}
