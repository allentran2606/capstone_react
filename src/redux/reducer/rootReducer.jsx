import { combineReducers } from "redux";
import { carouselReducer } from "./carouselReducer";
import { cumRapReducer } from "./cumRapReducer";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({
  userReducer,
  carouselReducer,
  cumRapReducer,
});
