import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const cumRapService = {
  getHeThongRapChieu: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinHeThongRap`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
